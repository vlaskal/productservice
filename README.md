Product service 
====================
This project is created to review possible implementation based on request documentation.
Contains one solution and two main projects.

Description
----------------
Product service offer few endpoints to maintain product catalog.
Main documentation is available in Swagger 2.0 format.
There is available UI to test endpoints in specific versions on url http://localhost:5000/swagger or https://loclahost:5001/swagger

Requested endpoints are implemented in version 3 of API http://localhost:5000/swagger/index.html?urls.primaryName=V3

Configuration
--------------
Copnfiguration file `appsettings.json` is located in folder `c:\<repo folder>\ProductCatalog\ProductCatalog`. Its basic structure is following
~~~~
{
  "Database": {
    "ConnectionString": "Server=localhost;Database=ProductCatalog;Trusted_Connection=True;",
    "UseInMemory": true
  },
  ....
}
~~~~
`ConnectionString` should be set to proper SQL server. Database will be created automatically together with populated data from seed. In case that databse will be reused, then seeder will not be applied.

`UseInMemory` is switch to use inmemory database instead of proper SQL Server. Inmemory database is automatically populated by data from seeder.

Technology stack
-------------------
* C#
* ASP.NET Core 2.1.0
* Entity Framework Core 2.1
* SwashBuckle
* xUnit

There is not need to explicitly install any dependency.
As IDE use Visual Studio 2017, VIsual Studio Code or any text editor.

Build, Test and Run from Visual Studio
--------------
### Prerequisities
To be able to build and run project you need to install .NET CORE SDK 2.1.x from (https://www.microsoft.com/net/download) and of course Visual Studio.

### Open and Run
Open solution file `ProductCatalog.sln` 

Select proper launch option from IIS Express or Kestrel and click Run.
Default page with requesed endpoint will be automatically opened

Build, Test and Run from command line
--------------
### Prerequisities
To be able to build and run project you need to install .NET CORE SDK 2.1.x from (https://www.microsoft.com/net/download)

### Build
In folder <repo folder>\ProductCatalog execute `c:\<repo folder>\ProductCatalog> dotnet build`

### Run
In folder <repo folder>/ProductCatalog execute `c:\<repo folder>\ProductCatalog> dotnet run`

### Test
In folder <repo folder>/ProductCatalog.Tests execute `c:\<repo folder>\ProductCatalog.Tests> dotnet test -v n`
