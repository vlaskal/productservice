﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductCatalog.Domain.Models
{
    public class Product
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public Uri ImageUri { get; set; }

        public decimal Price { get; set; }

        public string Description { get; set; }
    }
}
