﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductCatalog.Configurations.Models
{
    public class DatabaseConfiguration
    {
        public string ConnectionString { get; set; }

        public bool UseInMemory { get; set; }
    }
}
