﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProductCatalog.Configurations.Models;

namespace ProductCatalog.Configurations
{
    public static class ServiceCollectionExtensions
    {
        public static void AddConfiguredOptions(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddOptions();

            services.Configure<DatabaseConfiguration>(configuration.GetSection(ConfigurationSections.Database));
        }
    }
}
