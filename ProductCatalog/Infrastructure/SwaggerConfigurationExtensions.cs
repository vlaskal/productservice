﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;

namespace ProductCatalog.Infrastructure
{
    public static class SwaggerConfigurationExtensions
    {
        public static void AddConfiguredSwashBuckle(this IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                using (var provider = services.BuildServiceProvider())
                {
                    var apiVersionProvider = provider.GetRequiredService<IApiVersionDescriptionProvider>();

                    foreach (var description in apiVersionProvider.ApiVersionDescriptions)
                    {
                        options.SwaggerDoc(description.GroupName, CreateInfoForApiVersion(description));
                    }

                    //TODO: Possible bug in SwaggerGen so let see if really needs to be fixed by this
                    //options.OperationFilter<SwaggerDefaultValues>();
                    options.IncludeXmlComments(XmlCommentsFilePath);
                }
            });
        }

        public static void UseSwashBuckle(this IApplicationBuilder app)
        {
            app.UseSwagger();

            var apiVersionProvider = app.ApplicationServices.GetRequiredService<IApiVersionDescriptionProvider>();

            app.UseSwaggerUI(options =>
            {
                foreach (var description in apiVersionProvider.ApiVersionDescriptions)
                {
                    options.SwaggerEndpoint(SwaggerEndpoint(description.GroupName), description.GroupName.ToUpperInvariant());
                }
            });
        }

        private static readonly Func<string, string> SwaggerEndpoint = (version) => $"/swagger/{version}/swagger.json";

        private static Info CreateInfoForApiVersion(ApiVersionDescription description)
        {
            var info = new Info()
            {
                Title = $"Product catalog API v{description.ApiVersion}",
                Version = description.ApiVersion.ToString(),
                Description = "A sample application with Swagger, Swashbuckle, and API versioning.",
                Contact = new Contact() { Name = "Vlastimil Kaluza", Email = "vlastimil.kaluza@email.cz" },
                TermsOfService = "Interview only :)",
                License = new License() { Name = "MIT", Url = "https://opensource.org/licenses/MIT" }
            };

            if (description.IsDeprecated)
            {
                info.Description += " This API version has been deprecated.";
            }

            return info;
        }


        static string XmlCommentsFilePath
        {
            get
            {
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var fileName = typeof(Startup).GetTypeInfo().Assembly.GetName().Name + ".xml";
                return Path.Combine(basePath, fileName);
            }
        }
    }
}
