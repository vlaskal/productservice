﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProductCatalog.Persistence.Models;

namespace ProductCatalog.Persistence
{
    public static class ProductCatalogSeeder
    {
        public static readonly IEnumerable<Product> ProductData = new[]
        {
            new Product { Id = new Guid("C67A2C10-00FD-40AE-8FA6-E4D56E72D391"), Name = "Product A", ImageUri = "http://localhost", Price = 10M, Description = "Description of Product A." },
            new Product { Id = new Guid("1C1EF1E8-07F3-4DBD-A6F8-33109D9BE87E"), Name = "Product B", ImageUri = "http://localhost", Price = 20M, Description = "Description of Product B." },
            new Product { Id = new Guid("E43D28B2-5C98-41D0-B1A0-19055B13D2BB"), Name = "Product C", ImageUri = "http://localhost", Price = 30M, Description = "Description of Product C." },
            new Product { Id = new Guid("9826E568-F486-4298-BAF5-2FA029C2DB3C"), Name = "Product D", ImageUri = "http://localhost", Price = 40M, Description = "Description of Product D." }
        };

        public static void SeedProductCatalog(this ProductCatalogContext context)
        {
            if (context.Products.Any())
            {
                return;
            }

            context.Products.AddRange(ProductData);

            context.SaveChanges();
        }
    }
}
