﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using ProductCatalog.Configurations.Models;

namespace ProductCatalog.Persistence
{
    public static class ServiceCollectionExtensions
    {
        private static readonly string InMemoryDdatabaseName = "InMemory";

        public static void AddPersistenceLayer(this IServiceCollection services)
        {
            using (var provider = services.BuildServiceProvider())
            {
                var configuration = provider.GetRequiredService<IOptions<DatabaseConfiguration>>().Value;

                services.AddDbContext<ProductCatalogContext>(options => options.GetDatabaseOptions(configuration),
                                                             configuration.UseInMemory ? ServiceLifetime.Singleton : ServiceLifetime.Scoped);                
            }
        }

        private static void GetDatabaseOptions(this DbContextOptionsBuilder options, DatabaseConfiguration configuration)
        {
            if (configuration.UseInMemory)
            {
                options.UseInMemoryDatabase(InMemoryDdatabaseName);
            }
            else
            {
                if (String.IsNullOrWhiteSpace(configuration.ConnectionString))
                {
                    throw new InvalidOperationException("Database connection string is not setup correctly. Is empty, null or contains white spaces.");
                }

                options.UseSqlServer(configuration.ConnectionString);
            }

            options.EnableSensitiveDataLogging();           
        }
    }
}
