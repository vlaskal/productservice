﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using ProductCatalog.Configurations.Models;

namespace ProductCatalog.Persistence
{
    public static class ApplicationBuilderExtensions
    {
        public static void EnsurePersistence(this IApplicationBuilder app)
        {
            var configuration = app.ApplicationServices.GetRequiredService<IOptions<DatabaseConfiguration>>().Value;
            
            if (configuration.UseInMemory)
            {
                app.EnsureInMemoryPersistence();
            }
            else
            {
                app.EnsureRelationalPersistence();
            }
        }

        private static void EnsureInMemoryPersistence(this IApplicationBuilder app)
        {
            var context = app.ApplicationServices.GetRequiredService<ProductCatalogContext>();

            context.Database.EnsureCreated();
            context.SeedProductCatalog();
        }

        private static void EnsureRelationalPersistence(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<ProductCatalogContext>();

                context.Database.Migrate();
                context.SeedProductCatalog();
            }
        }
    }
}