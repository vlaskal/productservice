﻿using Microsoft.AspNetCore.Razor.Language.Intermediate;

namespace ProductCatalog.Repositories.Contracts
{
    public interface IMapper<TDomain, TPersistence> 
        where TDomain :  class, new() 
        where TPersistence : class, new()
    {
        TPersistence MapToPersistenceModel(TDomain from);

        void MapToPersistenceModel(TDomain from, TPersistence to);

        TDomain MapFromPersistenceModel(TPersistence from);

        void MapFromPersistenceModel(TPersistence from, TDomain to);
    }
}