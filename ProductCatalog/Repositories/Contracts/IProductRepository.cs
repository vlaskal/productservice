﻿using System;
using System.Collections.Generic;
using ProductCatalog.Domain.Models;

namespace ProductCatalog.Repositories.Contracts
{
    public interface IProductRepository
    {
        IEnumerable<Product> Find();

        Product Get(Guid id);

        Product Create(Product product);

        void Update(Product product);
        void Delete(Guid id);
    }
}