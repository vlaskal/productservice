﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProductCatalog.Persistence.Models;
using ProductCatalog.Repositories.Contracts;
using DomainProduct = ProductCatalog.Domain.Models.Product;

namespace ProductCatalog.Repositories.Mappers
{
    public class ProductMapper : BaseMapper_2<DomainProduct, Product>
    {
        public override void MapToPersistenceModel(DomainProduct @from, Product to)
        {
            to.Id = @from.Id;
            to.Name = @from.Name;
            to.Price = @from.Price;
            to.ImageUri = @from.ImageUri?.ToString();
            to.Description = @from.Description;
        }

        public override void MapFromPersistenceModel(Product @from, DomainProduct to)
        {
            to.Id = @from.Id;
            to.Name = @from.Name;
            to.Price = @from.Price ?? 0M;
            to.ImageUri = (@from.ImageUri == null) ? null : new Uri(@from.ImageUri);
            to.Description = @from.Description;
        }
    }
}
