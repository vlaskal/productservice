﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProductCatalog.Repositories.Contracts;

namespace ProductCatalog.Repositories.Mappers
{
    public abstract class BaseMapper_2<TDomain, TPersistence> : IMapper<TDomain, TPersistence> 
        where TDomain : class, new()
        where TPersistence: class, new()
    {
        public virtual TPersistence MapToPersistenceModel(TDomain @from)
        {
            var result = new TPersistence();
            MapToPersistenceModel(@from, result);
            return result;
        }

        public virtual TDomain MapFromPersistenceModel(TPersistence @from)
        {
            var result = new TDomain();
            MapFromPersistenceModel(@from, result);
            return result;
        }

        public abstract void MapToPersistenceModel(TDomain @from, TPersistence to);
        
        public abstract void MapFromPersistenceModel(TPersistence @from, TDomain to);
    }
}
