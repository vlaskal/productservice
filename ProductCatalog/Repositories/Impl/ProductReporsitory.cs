﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.OAuth.Claims;
using ProductCatalog.Persistence;
using ProductCatalog.Persistence.Models;
using ProductCatalog.Repositories.Contracts;
using DomainProduct = ProductCatalog.Domain.Models.Product;

namespace ProductCatalog.Repositories.Impl
{
    public class ProductReporsitory : IProductRepository
    {
        private readonly ProductCatalogContext _context;
        private readonly IMapper<DomainProduct, Product> _mapper;

        public ProductReporsitory(ProductCatalogContext context, IMapper<DomainProduct, Product> mapper)
        {
            _mapper = mapper;
            _context = context;
        }

        public IEnumerable<DomainProduct> Find()
        {
            return _context.Products.Select(_mapper.MapFromPersistenceModel);
        }

        public DomainProduct Get(Guid id)
        {
            return _mapper.MapFromPersistenceModel(GetFromPersistence(id));
        }

        public DomainProduct Create(DomainProduct product)
        {
            var dbProduct = _mapper.MapToPersistenceModel(product);

            Validate(dbProduct);

            _context.Products.Add(dbProduct);
            _context.SaveChanges();

            return Get(dbProduct.Id);
        }

        public void Update(DomainProduct product)
        {
            var dbProduct = GetFromPersistence(product.Id);

            _mapper.MapToPersistenceModel(product, dbProduct);

            Validate(dbProduct);

            _context.SaveChanges();
        }

        public void Delete(Guid id)
        {
            var dbProduct = GetFromPersistence(id);

            _context.Remove(dbProduct);
            _context.SaveChanges();
        }

        private Product GetFromPersistence(Guid id)
        {
            return _context.Products.Single(x => x.Id == id);
        }

        private static void Validate(Product product)
        {
            var validationContext = new ValidationContext(product);
            Validator.ValidateObject(product, validationContext);
        }


    }
}
