﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using ProductCatalog.Persistence.Models;
using ProductCatalog.Repositories.Contracts;
using ProductCatalog.Repositories.Impl;
using ProductCatalog.Repositories.Mappers;

namespace ProductCatalog.Repositories
{
    public static class ServiceCollectionExtensions
    {
        public static void AddRepositoryLayer(this IServiceCollection services)
        {
            services.AddRepositories();
            services.AddMappers();
        }

        private static void AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IProductRepository, ProductReporsitory>();
        }

        private static void AddMappers(this IServiceCollection services)
        {
            services.AddTransient<IMapper<Domain.Models.Product, Product>, ProductMapper>();
        }
    }
}
