﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using ProductCatalog.Api.Contracts;
using ProductCatalog.Api.Mappers;

namespace ProductCatalog.Api
{
    public static class ServiceCollectionExtensions
    {
        public static void AddApiLayer(this IServiceCollection services)
        {
            services.AddMappers();
            services.AddVersioning();
        }

        private static void AddMappers(this IServiceCollection services)
        {
            services.AddTransient<IProductMapper, ProductMapper>();
        }

        private static void AddVersioning(this IServiceCollection services)
        {
            services.AddVersionedApiExplorer(options =>
            {
                options.GroupNameFormat = "'v'V";
                options.SubstituteApiVersionInUrl = true;
            });

            services.AddApiVersioning(options => options.ReportApiVersions = true);
        }
    }
}
