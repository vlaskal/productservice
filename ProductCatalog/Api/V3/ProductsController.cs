﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProductCatalog.Api.Contracts;
using ProductCatalog.Api.Models;
using ProductCatalog.Repositories;
using ProductCatalog.Repositories.Contracts;

namespace ProductCatalog.ApiV3
{
    [ApiVersion("3")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductRepository _productRepository;
        private readonly IProductMapper _productMapper;

        public ProductsController(IProductRepository productRepository, IProductMapper productMapper)
        {
            _productMapper = productMapper;
            _productRepository = productRepository;
        }

        /// <summary>
        /// Retrieves all products
        /// </summary> 
        /// <returns>Set of Products</returns>
        /// <response code="200">Products successfully retrieved.</response>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ProductResponse>), 200)]
        public ActionResult<IEnumerable<ProductResponse>> Get()
        {
            return Ok(_productRepository.Find().Select(_productMapper.MapFromDomain));
        }

        /// <summary>
        /// Retrieves product based on provided id
        /// </summary> 
        /// <returns>Requested Product with Id</returns>
        /// <response code="200">Product successfully retrieved.</response>
        /// <response code="400">Provided Id did not meet validation rules.</response>
        /// <response code="404">Product with Id was not found.</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ProductResponse), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public ActionResult<ProductResponse> Get([FromRoute][Required] Guid? id)
        {
            if (!ModelState.IsValid)
            {
                return ValidationProblem(ModelState);
            }

            try
            {
                var product = _productRepository.Get(id.Value);
                var result = _productMapper.MapFromDomain(product);
                return Ok(result);
            }
            catch (InvalidOperationException ex)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Create new product
        /// </summary> 
        /// <returns>Created new product</returns>
        /// <response code="201">Product successfully created.</response>
        /// <response code="400">Body content did not meet validation rules.</response>
        [HttpPost]
        [ProducesResponseType(typeof(ProductResponse), 201)]
        [ProducesResponseType(400)]
        public ActionResult<ProductResponse> Post([FromBody][Required] ProductRequest productRequest)
        {
            if (!ModelState.IsValid)
            {
                return ValidationProblem(ModelState);
            }

            try
            {
                var product = _productMapper.MapFromProductRequest(productRequest);
                var createdProduct = _productRepository.Create(product);
                var result = _productMapper.MapFromDomain(createdProduct);
                return Created(new Uri($"api/v3/products/{result.Id}", UriKind.Relative) , result);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.ValidationResult.ErrorMessage);
            }
        }

        /// <summary>
        /// Update whole product record
        /// </summary>
        /// <response code="204">Product successfully updated.</response>
        /// <response code="400">Provided Id or body content did not meet validation rules.</response>
        /// <response code="404">Product with Id was not found.</response>
        [HttpPut("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public ActionResult Put([FromRoute][Required] Guid? id, [FromBody][Required] ProductRequest productRequest)
        {
            if (!ModelState.IsValid)
            {
                return ValidationProblem(ModelState);
            }

            try
            {
                var product = _productMapper.MapFromProductRequest(productRequest);
                product.Id = id.Value;
                _productRepository.Update(product);
                return NoContent();
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.ValidationResult.ErrorMessage);
            }
            catch (InvalidOperationException ex)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Delete product
        /// </summary>
        /// <response code="204">Product successfully deleted.</response>
        /// <response code="400">Provided Id did not meet validation rules.</response>
        /// <response code="404">Product with Id was not found.</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public ActionResult Delete([FromRoute][Required] Guid? id)
        {
            if (!ModelState.IsValid)
            {
                return ValidationProblem(ModelState);
            }

            try
            {
                _productRepository.Delete(id.Value);
                return NoContent();
            }
            catch (InvalidOperationException ex)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Partial update of product record by description field
        /// </summary>
        /// <response code="200">Product successfully updated.</response>
        /// <response code="400">Provided Id or body content did not meet validation rules.</response>
        /// <response code="404">Product with Id was not found.</response>
        [HttpPatch("{id}")]
        [ProducesResponseType(typeof(ProductResponse), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public ActionResult Patch([FromRoute][Required] Guid? id, [FromBody][Required] ProductPatch productPatch)
        {
            if (!ModelState.IsValid)
            {
                return ValidationProblem(ModelState);
            }

            try
            {
                var product = _productRepository.Get(id.Value);
                _productMapper.MapFromProductPatch(productPatch, product);
                _productRepository.Update(product);
                return Ok(product);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.ValidationResult.ErrorMessage);
            }
            catch (InvalidOperationException ex)
            {
                return NotFound();
            }
        }
    }
}
