﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ProductCatalog.ApiV1
{
    [ApiVersion("1", Deprecated = true)]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private static List<string> data = new List<string>() { "value1", "value2" };

        // GET api/v1/values
        [HttpGet]
        public ActionResult Get()
        {
            return Ok(data);
        }

        // POST api/v1/values
        [HttpPost]
        public ActionResult Post([FromBody] string value)
        {
            data.Add(value);
            return Created($"/api/v1/values/{data.Count-1}", value);
        }

        // DELETE api/v1/values/5
        [HttpDelete("{id}")]
        public ActionResult Delete([FromRoute] int id)
        {
            data.RemoveAt(id);
            return Accepted();
        }
    }
}
