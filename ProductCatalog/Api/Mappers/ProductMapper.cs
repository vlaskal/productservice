﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProductCatalog.Api.Contracts;
using ProductCatalog.Api.Models;
using DomainProduct = ProductCatalog.Domain.Models.Product;

namespace ProductCatalog.Api.Mappers
{
    public class ProductMapper : IProductMapper
    {
        public ProductResponse MapFromDomain(DomainProduct @from)
        {
            var result = new ProductResponse();
            MapFromDomain(@from, result);
            return result;
        }

        public DomainProduct MapFromProductRequest(ProductRequest product)
        {
            var result = new DomainProduct();
            MapToDomain(product, result);
            return result;
        }

        public void MapFromProductPatch(ProductPatch productPatch, DomainProduct product)
        {
            product.Description = productPatch.Description;
        }

        private void MapFromDomain(DomainProduct @from, ProductResponse to)
        {
            to.Id = @from.Id;
            to.Name = @from.Name;
            to.Price = @from.Price;
            to.ImageUri = @from.ImageUri?.ToString();
            to.Description = @from.Description;
        }

        private void MapToDomain(ProductRequest @from, DomainProduct to)
        {
            to.Name = @from.Name;
            to.Price = @from.Price ?? 0M;
            to.ImageUri = (@from.ImageUri == null) ? null : new Uri(@from.ImageUri);
            to.Description = @from.Description;
        }
    }
}
