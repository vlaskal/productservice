﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ProductCatalog.Controllers
{
    [ApiVersion("2")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private static List<string> data = new List<string>() { "value1", "value2" };

        // GET api/v2/values
        [HttpGet]
        public ActionResult Get()
        {
            return Ok(data);
        }

        // GET api/v2/values/5
        [HttpGet("{id}")]
        public ActionResult Get([FromRoute] int id)
        {
            return Ok(data[id]);
        }

        // POST api/v2/values
        [HttpPost]
        public ActionResult Post([FromBody] string value)
        {
            data.Add(value);
            return Created($"/api/v2/values/{data.Count-1}", value);
        }

        // PUT api/v2/values/5
        [HttpPut("{id}")]
        public ActionResult Put([FromRoute] int id, [FromBody] string value)
        {
            data[id] = value;
            return Accepted();
        }

        // DELETE api/v2/values/5
        [HttpDelete("{id}")]
        public ActionResult Delete([FromRoute] int id)
        {
            data.RemoveAt(id);
            return Accepted();
        }
    }
}
