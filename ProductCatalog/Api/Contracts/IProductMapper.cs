﻿using ProductCatalog.Api.Models;
using DomainProduct = ProductCatalog.Domain.Models.Product;

namespace ProductCatalog.Api.Contracts
{
    public interface IProductMapper
    {
        ProductResponse MapFromDomain(DomainProduct product);
        DomainProduct MapFromProductRequest(ProductRequest product);
        void MapFromProductPatch(ProductPatch productPatch, DomainProduct product);
    }
}