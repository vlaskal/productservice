﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProductCatalog.Api.Models
{
    /// <summary>
    /// Represent model used as response from any GET, POST or PATCH requests
    /// </summary>
    public class ProductResponse
    {
        /// <summary>
        /// Unique product id
        /// </summary>
        [Required]
        public Guid Id { get; set; }

        /// <summary>
        /// Product name
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        /// <summary>
        /// Url of product image
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        [Url]
        public string ImageUri { get; set; }

        /// <summary>
        /// Product price
        /// </summary>
        [Required]
        public decimal? Price { get; set; }

        /// <summary>
        /// Optional product description
        /// </summary>
        public string Description { get; set; }
    }
}
