﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProductCatalog.Api.Models
{
    /// <summary>
    /// Represent model used in any POST or PUT requests
    /// </summary>
    public class ProductRequest
    {
        /// <summary>
        /// Product name.
        /// </summary>
        /// <remarks>Empty string or null is not allowed.</remarks>
        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        /// <summary>
        /// Url of product image
        /// </summary>
        /// <remarks>Empty string or null is not allowed.</remarks>
        [Required(AllowEmptyStrings = false)]
        [Url]
        public string ImageUri { get; set; }

        /// <summary>
        /// Product price
        /// </summary>
        /// <remarks>Value has to be provided</remarks>
        [Required]
        public decimal? Price { get; set; }

        /// <summary>
        /// Optional product description
        /// </summary>
        public string Description { get; set; }
    }
}
