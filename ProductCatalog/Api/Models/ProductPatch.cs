﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductCatalog.Api.Models
{
    /// <summary>
    /// Represent model used in any PATCH request
    /// </summary>
    public class ProductPatch
    {
        /// <summary>
        /// Optional product description
        /// </summary>
        public string Description { get; set; }
    }
}
