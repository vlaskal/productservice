using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Moq;
using ProductCatalog.Persistence;
using ProductCatalog.Domain.Models;
using ProductCatalog.Repositories.Contracts;
using ProductCatalog.Tests.RepositoryTests.Fixtures;
using Xunit;

namespace ProductCatalog.Tests.RepositoryTests
{
    public class ProductRepositoryTests : IClassFixture<ProductRepositoryTestFixture>
    {
        private readonly ProductRepositoryTestFixture _fixture;

        public ProductRepositoryTests(ProductRepositoryTestFixture fixture)
        {
            _fixture = fixture;
            _fixture.ResetDatabase();
        }

        [Fact]
        public void FindShouldReturnOneProduct()
        {
            var repository = _fixture.Resolve<IProductRepository>();

            var actual = repository.Find().ToList();

            Assert.Single(actual);
            Assert.Equal(_fixture.ProductZ.Id, actual.Single().Id);
        }

        [Fact]
        public void GetShouldReturnCorrectProduct()
        {
            var repository = _fixture.Resolve<IProductRepository>();

            var actual = repository.Get(_fixture.ProductZ.Id);

            Assert.NotNull(actual);
            Assert.Equal(_fixture.ProductZ.Id, actual.Id);
            Assert.Equal(_fixture.ProductZ.Name, actual.Name);                       
        }

        [Fact]
        public void GetShouldThrowExceptionWhenNotFound()
        {
            var repository = _fixture.Resolve<IProductRepository>();

            Assert.Throws<InvalidOperationException>(() => repository.Get(Guid.Empty));
        }

        [Fact]
        public void CreateShouldAddProduct()
        {
            var repository = _fixture.Resolve<IProductRepository>();
            var product = _fixture.ProductX();

            repository.Create(product);
            
            Assert.Equal(2, repository.Find().Count());
        }

        [Fact]
        public void CreateShouldThrowExcpetionOnValidationError()
        {
            var repository = _fixture.Resolve<IProductRepository>();
            var product = _fixture.NullProduct;

            Assert.Throws<ValidationException>(() => repository.Create(product));            
            Assert.Single(repository.Find());
            Assert.Equal(_fixture.ProductZ.Id, repository.Find().Single().Id);
        }

        [Fact]
        public void UpdateShouldUpdateDbItem()
        {
            var repository = _fixture.Resolve<IProductRepository>();

            var product = _fixture.ProductX();
            product.Id = _fixture.ProductZ.Id;

            repository.Update(product);

            Assert.Single(repository.Find());
            Assert.Equal(_fixture.ProductZ.Id, repository.Find().Single().Id);
            Assert.Equal(product.Name, repository.Get(_fixture.ProductZ.Id).Name);
        }

        [Fact]
        public void UpdateShouldUThrowExcpetionOnValidationError()
        {
            var repository = _fixture.Resolve<IProductRepository>();

            var product = _fixture.NullProduct;
            product.Id = _fixture.ProductZ.Id;

            Assert.Throws<ValidationException>(() => repository.Update(product));
            Assert.Single(repository.Find());
            Assert.Equal(_fixture.ProductZ.Id, repository.Find().Single().Id);
        }

        [Fact]
        public void UpdateShouldUThrowExcpetionWhenNotFound()
        {
            var repository = _fixture.Resolve<IProductRepository>();

            var product = _fixture.ProductX();
            product.Id = Guid.NewGuid();

            Assert.Throws<InvalidOperationException>(() => repository.Update(product));
            Assert.Single(repository.Find());
            Assert.Equal(_fixture.ProductZ.Id, repository.Find().Single().Id);
        }

        [Fact]
        public void DeleteShouldUDeleteDbItem()
        {
            var repository = _fixture.Resolve<IProductRepository>();

            repository.Delete(_fixture.ProductZ.Id);

            Assert.Empty(repository.Find());
        }

        [Fact]
        public void DeleteShouldThrowExceptionWhenNotFound()
        {
            var repository = _fixture.Resolve<IProductRepository>();

            Assert.Throws<InvalidOperationException>(() => repository.Delete(Guid.NewGuid()));
            Assert.Single(repository.Find());
        }
    }
}
