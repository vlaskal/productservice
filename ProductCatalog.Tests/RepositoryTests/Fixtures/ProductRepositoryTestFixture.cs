using System;
using System.Linq;
using ProductCatalog.Persistence;
using DbProduct = ProductCatalog.Persistence.Models.Product;
using ProductCatalog.Domain.Models;

namespace ProductCatalog.Tests.RepositoryTests.Fixtures
{
    public class ProductRepositoryTestFixture : TestContext
    {
        public readonly Product NullProduct = new Product
        {
            Name = null,
            Price = 0M,
            ImageUri = null,
            Description = null
        };

        public readonly Func<Product> ProductX = () => new Product
        {
            Id = Guid.NewGuid(),
            Name = "Product X",
            Price = 100M,
            ImageUri = new Uri("http://localhost/x.jpg"),
            Description = "Description of Product X"
        };

        public readonly DbProduct ProductZ = new DbProduct()
        {
            Id = Guid.NewGuid(),
            Name = "Product Z",
            Price = 10M,
            ImageUri = "http://localhost/z.jpg"
        };

        public void ResetDatabase()
        {
            var dbContext = Resolve<ProductCatalogContext>();

            dbContext.RemoveRange(dbContext.Products.ToList());
            dbContext.SaveChanges();

            dbContext.Products.Add(ProductZ);
            dbContext.SaveChanges();
        }        
    }
}