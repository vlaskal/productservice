﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace ProductCatalog.Tests
{
    public class JsonContent : StringContent
    {
        public JsonContent(object content)
            : this(JsonConvert.SerializeObject(content))
        {
        }

        public JsonContent(string content)
            : base(content, Encoding.UTF8, "application/json")
        {
        }
    }

    public class JsonContent<T> : JsonContent
    {
        private static string Serialize(T content)
        {
            var writer = new MemoryTraceWriter();

            var settings = new JsonSerializerSettings();
            settings.TraceWriter = writer;

            var result = JsonConvert.SerializeObject(content, typeof(T), settings);

            Debug.WriteLine(writer);

            return result;
        }

        public JsonContent(T content)
            : base(Serialize(content))
        {
        }
    }
}
