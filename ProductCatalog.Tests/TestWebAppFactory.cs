﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;

namespace ProductCatalog.Tests
{
    public class TestWebAppFactory : WebApplicationFactory<Startup>
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureAppConfiguration(configurationBuilder =>
            {
                configurationBuilder.AddInMemoryCollection(new[]
                {
                    new KeyValuePair<string, string>("Database:ConnectionString", ""),
                    new KeyValuePair<string, string>("Database:UseInMemory", "true")
                });
            });
        }
    }
}
