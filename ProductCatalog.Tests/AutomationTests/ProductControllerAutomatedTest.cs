﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using ProductCatalog.Api.Models;
using ProductCatalog.Persistence;
using Xunit;

namespace ProductCatalog.Tests.AutomationTests
{
    public class ProductControllerAutomatedTest : IClassFixture<TestWebAppFactory>
    {
        private readonly TestWebAppFactory _webAppFactory;

        private HttpClient HttpClient => _webAppFactory.CreateClient(new WebApplicationFactoryClientOptions()
        {
            BaseAddress = new Uri("http://localhost/api/v3/products/")
        });

        public ProductControllerAutomatedTest(TestWebAppFactory webAppFactory)
        {
            _webAppFactory = webAppFactory;
        }

        [Fact]
        public async Task CreateAndGetAndUpdateAndGetAndDeleteProductShouldWork()
        {
            var productToCreate = new ProductRequest()
            {
                Name = "Product Z",
                Price = 1000M,
                ImageUri = "http://localhost",
                Description = "Description of Product Z"
            };

            var response = await HttpClient.PostAsync(String.Empty, new JsonContent<ProductRequest>(productToCreate));
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);

            var content = await response.Content.ReadAsStringAsync();
            Assert.False(String.IsNullOrWhiteSpace(content));

            var productCreated = JsonConvert.DeserializeObject<ProductResponse>(content);
            Assert.NotNull(productCreated);

            productToCreate.Name = "Product Z - Changed";
            response = await HttpClient.PutAsync(productCreated.Id.ToString(), new JsonContent<ProductRequest>(productToCreate));
            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);

            response = await HttpClient.GetAsync(productCreated.Id.ToString());
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            content = await response.Content.ReadAsStringAsync();
            Assert.False(String.IsNullOrWhiteSpace(content));

            var productUpdated = JsonConvert.DeserializeObject<ProductResponse>(content);
            Assert.NotNull(productCreated);
            Assert.NotEqual(productCreated.Name, productUpdated.Name);

            response = await HttpClient.DeleteAsync(productCreated.Id.ToString());
            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);

            response = await HttpClient.GetAsync(String.Empty);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            content = await response.Content.ReadAsStringAsync();
            Assert.False(String.IsNullOrWhiteSpace(content));

            var products = JsonConvert.DeserializeObject<IEnumerable<ProductResponse>>(content);
            Assert.NotNull(products);
            Assert.Equal(ProductCatalogSeeder.ProductData.Count(), products.Count());
        }
    }
}
