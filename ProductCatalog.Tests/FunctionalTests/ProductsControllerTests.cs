﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;
using Newtonsoft.Json;
using ProductCatalog.Api.Models;
using ProductCatalog.Persistence;
using DbProduct = ProductCatalog.Persistence.Models.Product;

namespace ProductCatalog.Tests.FunctionalTests
{
    public class ProductsControllerTests
    {
        private readonly TestWebAppFactory _webAppFactory;
        private readonly IEnumerable<DbProduct> _seederData = ProductCatalogSeeder.ProductData;

        private HttpClient HttpClient => _webAppFactory.CreateClient(new WebApplicationFactoryClientOptions()
                                                                    {
                                                                        BaseAddress = new Uri("http://localhost/api/v3/products/")
                                                                    });

        public ProductsControllerTests()
        {
            _webAppFactory = new TestWebAppFactory();
        }

        [Fact]
        public async Task GetShouldReturnData()
        {
            var response = await HttpClient.GetAsync(String.Empty);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var content = await response.Content.ReadAsStringAsync();
            Assert.False(String.IsNullOrWhiteSpace(content));

            var data = JsonConvert.DeserializeObject<IEnumerable<ProductResponse>>(content);         
            Assert.False(data == null);
            Assert.Equal(4, data.Count());     
            Assert.All(data, p => Assert.Contains(_seederData, s => s.Id == p.Id));
        }



        [Fact]
        public async Task GetByIdShouldReturnData()
        {
            var expected = _seederData.First();

            var response = await HttpClient.GetAsync(expected.Id.ToString());
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var content = await response.Content.ReadAsStringAsync();
            Assert.False(String.IsNullOrWhiteSpace(content));

            var data = JsonConvert.DeserializeObject<ProductResponse>(content);
            Assert.Equal(expected.Id, data.Id);
            Assert.Equal(expected.Name, data.Name);
        }

        [Fact]
        public async Task GetByIdWithWrongIdShouldReturnBadRequest()
        {
            var response = await HttpClient.GetAsync("blabol");
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task GetByIdWithNotExisitngIdShouldReturnNotFound()
        {
            var response = await HttpClient.GetAsync(Guid.NewGuid().ToString());
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }



        [Fact]
        public async Task PostShouldCreateAndReturnData()
        {
            var expected = new ProductRequest()
            {
                Name = "Product to Create",
                ImageUri = "https://localhost",
                Price = 123M
            };

            var response = await HttpClient.PostAsync(String.Empty, new JsonContent<ProductRequest>(expected));
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);

            var content = await response.Content.ReadAsStringAsync();
            Assert.False(String.IsNullOrWhiteSpace(content));

            var data = JsonConvert.DeserializeObject<ProductResponse>(content);
            Assert.NotEqual(Guid.Empty, data.Id);
            Assert.Equal(expected.Name, data.Name);
        }

        [Fact]
        public async Task PostWithWrongContentShouldReturnBadRequest()
        {
            var expected = new ProductRequest()
            {
                Name = null,
                ImageUri = "https://localhost",
                Price = 123M
            };

            var response = await HttpClient.PostAsync(String.Empty, new JsonContent<ProductRequest>(expected));
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }


        [Fact]
        public async Task PutByIdShouldUpdateProduct()
        {
            var expected = new ProductRequest()
            {
                Name = "Product to Update",
                ImageUri = "https://localhostek",
                Price = 123M
            };

            var productToUpdate = _seederData.First();

            var response = await HttpClient.PutAsync(productToUpdate.Id.ToString(), new JsonContent<ProductRequest>(expected));
            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);

            response = await HttpClient.GetAsync(productToUpdate.Id.ToString());
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var content = await response.Content.ReadAsStringAsync();
            Assert.False(String.IsNullOrWhiteSpace(content));

            var data = JsonConvert.DeserializeObject<ProductResponse>(content);
            Assert.Equal(productToUpdate.Id, data.Id);
            Assert.Equal(expected.Name, data.Name);
            Assert.Equal(expected.Price, data.Price);
        }

        [Fact]
        public async Task PutByIdWithWrongIdShouldReturnBadRequest()
        {
            var expected = new ProductRequest()
            {
                Name = "Product to Update",
                ImageUri = "https://localhostek",
                Price = 123M
            };            

            var response = await HttpClient.PutAsync("blabol", new JsonContent<ProductRequest>(expected));
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task PutByIdWithWrongContentShouldReturnBadRequest()
        {
            var expected = new ProductRequest()
            {
                Name = null,
                ImageUri = "https://localhostek",
                Price = 123M
            };

            var productToUpdate = _seederData.First();

            var response = await HttpClient.PutAsync(productToUpdate.Id.ToString(), new JsonContent<ProductRequest>(expected));
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task PutByIdWithNotExisitngIdShouldReturnNotFound()
        {
            var expected = new ProductRequest()
            {
                Name = "Product to Update",
                ImageUri = "https://localhostek",
                Price = 123M
            };

            var response = await HttpClient.PutAsync(Guid.NewGuid().ToString(), new JsonContent<ProductRequest>(expected));
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }


        [Fact]
        public async Task PatchByIdShouldUpdateProduct()
        {
            var productToUpdate = _seederData.First();

            var expected = new ProductPatch()
            {
                Description = productToUpdate.Description + " something more"
            };
            
            var response = await HttpClient.PatchAsync(productToUpdate.Id.ToString(), new JsonContent<ProductPatch>(expected));
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var content = await response.Content.ReadAsStringAsync();
            Assert.False(String.IsNullOrWhiteSpace(content));

            var data = JsonConvert.DeserializeObject<ProductResponse>(content);
            Assert.Equal(productToUpdate.Id, data.Id);
            Assert.Equal(productToUpdate.Name, data.Name);
            Assert.Equal(productToUpdate.ImageUri, data.ImageUri);
            Assert.Equal(productToUpdate.Price, data.Price);
            Assert.Equal(expected.Description, data.Description);
        }

        [Fact]
        public async Task PatchByIdWithWrongIdShouldReturnBadRequest()
        {
            var productToUpdate = _seederData.First();

            var expected = new ProductPatch()
            {
                Description = productToUpdate.Description + " something more"
            };

            var response = await HttpClient.PatchAsync("blabol" , new JsonContent<ProductPatch>(expected));
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task PatchByIdWithWrongContentShouldReturnBadRequest()
        {
            var productToUpdate = _seederData.First();

            var response = await HttpClient.PatchAsync(productToUpdate.Id.ToString(), new StringContent(String.Empty));
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task PatchByIdWithNotExisitngIdShouldReturnNotFound()
        {
            var productToUpdate = _seederData.First();

            var expected = new ProductPatch()
            {
                Description = productToUpdate.Description + " something more"
            };

            var response = await HttpClient.PatchAsync(Guid.NewGuid().ToString(), new JsonContent<ProductPatch>(expected));
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }


        [Fact]
        public async Task DeleteByIdShouldRemoveProduct()
        {
            var expected = _seederData.Last();

            var response = await HttpClient.DeleteAsync($"{expected.Id.ToString()}");
            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);

            var currentDataContent = await HttpClient.GetStringAsync(String.Empty);
            var data = JsonConvert.DeserializeObject<IEnumerable<ProductResponse>>(currentDataContent);
            Assert.False(data == null);
            Assert.Equal(3, data.Count());
            Assert.All(data, p => Assert.Contains(_seederData, s => s.Id == p.Id));
            Assert.All(data, p => Assert.True(p.Id != expected.Id));
        }

        [Fact]
        public async Task DeleteByIdWithWrongIdShouldReturnBadRequest()
        {
            var response = await HttpClient.DeleteAsync("blabol");
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task DeleteByIdWithNotExisitngIdShouldReturnNotFound()
        {
            var response = await HttpClient.DeleteAsync(Guid.NewGuid().ToString());
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
    }
}
