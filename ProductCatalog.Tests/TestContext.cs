﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Moq;

namespace ProductCatalog.Tests
{
    public class TestContext : IDisposable
    {
        private readonly Startup _appStartup;
        private readonly IServiceCollection _serviceCollection;
        private readonly Dictionary<Type, Mock> _mocks = new Dictionary<Type, Mock>();

        private IConfiguration _configuration;
        private ServiceProvider _serviceProvider = null;


        public TestContext()
        {
            _serviceCollection = new ServiceCollection();

            CreateConfiguration();

            _appStartup = new Startup(_configuration);            
            _appStartup.ConfigureServices(_serviceCollection);
        }
        
        public T Resolve<T>()
        {
            EnsureServiceProvider();
            return _serviceProvider.GetService<T>();
        }

        public Mock<T> GetMock<T>() where T : class
        {
            var type = typeof(T);

            if (_mocks.ContainsKey(type))
            {
                return _mocks[type] as Mock<T>;
            }

            var mock = new Mock<T>();
            _mocks.Add(type, mock);

            return mock;
        }

        public void Dispose()
        {
            _serviceProvider?.Dispose();
        }

        private void EnsureServiceProvider()
        {
            if (_serviceProvider != null)
            {
                return;
            }

            foreach (var pair in _mocks)
            {
                _serviceCollection.Remove(_serviceCollection.First(x => x.ServiceType == pair.Key)); //Not needed. For sure there is something to replace.
                _serviceCollection.AddSingleton(pair.Key, sp => pair.Value.Object);
            }

            _serviceProvider = _serviceCollection.BuildServiceProvider();
        }

        private void CreateConfiguration()
        {
            // Set up configuration sources.
            var builder = new ConfigurationBuilder()
                .AddInMemoryCollection(new[]
                {
                    new KeyValuePair<string, string>("Database:ConnectionString", ""),
                    new KeyValuePair<string, string>("Database:UseInMemory", "true")
                });

            _configuration = builder.Build();
            _serviceCollection.AddSingleton<IConfiguration>(_configuration);
        }
    }
}
